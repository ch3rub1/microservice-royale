package msroyale;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;

public class HttpVerticle extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		HttpServer server = vertx.createHttpServer();

		Router router = Router.router(vertx);

		router.route("/healthcheck").handler(routingContext -> {

			HttpServerResponse response = routingContext.response();
			response.headers().add("Content-Type", "application/json");
			response.end("{\"State\":\"OK\"}");
		});

		server.requestHandler(router::accept).listen(9000);
	}
}
